from random import randint
cr = int(input("What challenge rating creature do you want? "))
size1 = ['Tiny', 'Small', 'Medium', 'Large']
size6 = ['Medium', 'Large', 'Huge', 'Gargantuan']
size11 = ['Medium', 'Large', 'Huge', 'Gargantuan', 'Collosal']
size16 = ['Large', 'Huge', 'Gargantuan', 'Collosal']
subtype = ['Dragon', 'Aberation'] #'Humanoid ', 'Fey ', 'Plant ', 'Construct ']
attackdragon = ['Bite', 'Claws', 'Wings', 'Tail']
attackaberation = ['Bite', 'Claws', 'Tentacle', 'Slam']
typ = random.choice(subtype)
strength =10+2*cr
sizemod = 0
strengthmod = 0
attacks = 0
attacknumber = 1
attackfinal = ['']
ac = int(14+1.5*cr)
hp = cr*8
Bite=Claw=Tentacle=Tail=Slam=Wing= ""
creature_dict = {"Tiny":(-4,2,2,'1d3','1d2'),"Small":(-2,1,1,'1d4','1d3'),
"Medium":(0,0,0,'1d6','1d4'), "Large":(2,-1,-1,'1d8','1d6'),
"Huge":(4,-2,-2,'2d6','1d8'), "Gargantuan":(8,-3,-3,'2d8','2d6'),
"Collosal":(16,-4,-4,'4d6','2d8')}
if typ == 'Dragon ':
    attackfinal == attackdragon
    hp *= 2
elif typ == 'Fey ':
    attackfinal == attackfey
    hp *= .8
else:
    attackfinal == attackaberation
    
if cr <= 5:
    size = random.choice(size1)
elif cr >=6 and cr <11:
    size = random.choice(size6)
    attacknumber += 1
elif cr >= 11 and cr <16:
    size = random.choice(size11)
    attacknumber += 2
else:
    size = random.choice(size16)
    attacknumber += 3

strength += creature_dict[size][0]
ac += creature_dict[size][1]
size_mod += creature_dict[size][2]
Bite=Tail = creature_dict[size][3]
Claw=Tentacle=Wing=Slam= creature_dict[size][4]

strengthmod = int((strength-10)/2)
attackbonus = int(cr+strengthmod+sizemod)

print('Your creature is a ', size, typ)
print("The monster's AC is {} with an attack bonus of {} and {} health".format(ac,attack_bonus,hp))
print("If it has any special abilities, the DC is ",int(10+cr*1.2))
while attacks < attacknumber:
    drop = random.choice(attackfinal)
    attack = ''
    if drop == "Bite" or "Tail":
        attack == Bite
    if drop == "Claw" or "Tentacle" or "Wing" or "Slam":
        attack == Claw
    print("The creature's {} danage us {}+{}".format(drop,attack,strength_mod))
    attackfinal.remove(drop)
    attacks+=1
